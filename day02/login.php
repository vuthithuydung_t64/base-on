<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
echo "<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>
    <style>
    .container{
        height: 300px;
        width: 600px;
        border: 2px solid rgb(40, 91, 185);
        margin: 0 auto;
        display: flex;
        flex-direction: column;
        justify-content: center;
    }
    .m-header{
        background-color: gainsboro;
        width: 368px;
        margin: 0 auto;
        padding: 5px 5px 5px 10px;
        margin-bottom: 15px;
        height: 25px;
        line-height: 25px;
    }
    .m-row{
        display: flex;
        justify-content: center;
        margin-bottom: 10px;
    }
    .label{
        border: 1px solid rgb(40, 91, 185);
        padding: 5px;
        margin-right: 15px;
        background-color: rgb(86 141 245);
        width: 150px;
        height: 25px;
        line-height: 25px;
        color: white;
    }
    .m-input{
        outline: none;
        border: 1px solid rgb(40, 91, 185);
        border-radius: 2px;
        width: 200px;
    }
    .btn{
        padding: 10px 25px;
        border-radius: 4px;
        background-color: rgb(86 141 245);
        border: 1px solid rgb(40, 91, 185);
        cursor: pointer;
        color: white;
        margin-top: 30px;
    }
    </style>
</head>
<body>
    <form action=''>
        <div class='container'>
            <div class='m-header'>
            Bây giờ là: " . date("H:i d/m/Y") . "
            </div>
            <div class='m-row'>
                <div class='label'>Tên đăng nhập</div>
                <input class='m-input' type='text'>
            </div>
            <div class='m-row'>
                <div class='label'>Mật khẩu</div>
                <input class='m-input' type='password'>
            </div>
            <div class='m-row'><input type='submit' value='Đăng nhập' class='btn'></input></div>
        </div>
    </form>
</body>

</html>";